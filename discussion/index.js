// console.log('Hello From JS');

// DOM Selectors

// first, you have to describe the location where it is going to target/access certain elements

// identify the attribute and value to properly recognize and identify which element to target
// visualize the statement in javascript
// let/const jsObject = {}

// [SECTION] querySelector
// const firstName = document.querySelector('#firstName');
// const lastName = document.querySelector('#lastName');

// [SECTION] getElement
// getElementById => target a single component
const firstName = document.getElementById('firstName')
const lastName = document.getElementById('lastName')

// getElementsbyClassName => can be essential when targeting multiple components at the same time
const inputFields = document.getElementsByClassName('form-control');

// getElementsbyTagName => can be used when targeting elements of the same tags.
const heading = document.getElementsByTagName('h3')

// check if you were able to successfully target an element from the document.
console.log(firstName);
console.log(lastName);
console.log(inputFields);
console.log(heading);

// check type of data that we targeted from the document.
// console.log(typeof firstName);

// get the information from the input fields
// target the value property of the object


